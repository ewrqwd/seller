package com.example.micro_start.store.Controlador;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SharedPreferencesUsuario {

    //nombre de shared prefef
    public static final String SHARED_PREF_NAME = "LOGIN";

    //Username
    public static final String USER_ID = "id";
    public static final String ROL_ID = "id_rol";
    public static final String ROL = "rol";
    public static final String NOMBRE = "nombre";
    public static final String APELLIDO = "apellido";
    public static SharedPreferencesUsuario mInstance;
    public static Context mCtx;


    //constructor, recibe como parametro un Context
    public SharedPreferencesUsuario(Context context) {
        mCtx = context;
    }


    public static synchronized SharedPreferencesUsuario getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPreferencesUsuario(context);
        }
        return mInstance;
    }


    //method to store user data
    public void GuardarUsuarioLogiado(JSONObject JSON) throws JSONException {
    ;
        android.content.SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_ID, String.valueOf((Integer) JSON.get("id")));
        editor.putString(ROL_ID,  String.valueOf((Integer) JSON.get("id_rol")));
        editor.putString(ROL,  String.valueOf((String) JSON.get("rol")));
        editor.putString(NOMBRE, String.valueOf((String) JSON.get("nombre")));
        editor.putString(APELLIDO, String.valueOf((String) JSON.get("apellido")));

        editor.commit();
    }

    public void CerrarCesssion() {
        android.content.SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_ID, null);
              editor.commit();
    }
    //check if user is logged in
    public boolean ValidarLogiado() {
        android.content.SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_ID, null) != null;
    }
    public static String BuscarDato(String Parametro) {
        android.content.SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(Parametro, Parametro) ;
    }
//
//
//    //find logged in user
//    //con esto podremos obtener los datos aue guardamos, y asi poder hacer uso de ellos donde los necesitemos
//    //por ejemplo estamos guardando el ID
//    //si llegaramos a necesitar el ID para hacer una consulta de ese usuario, una manera facil de obtenerlo es desde aqui
//    public HashMap<String, String> LoggedInUser() {
//        android.content.SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
//        HashMap<String, String> usuario = new HashMap<>();
//        usuario.put(USER_ID, sharedPreferences.getString(USER_ID, null));
//        return usuario;
//
//    }

}
