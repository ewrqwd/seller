package com.example.micro_start.store.Modelo;

public class SELECTOR {
    private  int index;
    private  String value;

    public SELECTOR(int index, String value) {
        this.index = index;
        this.value = value;
    }

    public SELECTOR() {
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
