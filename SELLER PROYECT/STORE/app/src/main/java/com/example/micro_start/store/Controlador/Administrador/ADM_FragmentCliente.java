package com.example.micro_start.store.Controlador.Administrador;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.micro_start.store.Controlador.SharedPreferencesUsuario;
import com.example.micro_start.store.Controlador.UTILITARIOS;
import com.example.micro_start.store.Modelo.SELECTOR;
import com.example.micro_start.store.Modelo.SISTEMA;
import com.example.micro_start.store.Modelo.USUARIO;
import com.example.micro_start.store.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SuppressLint("ValidFragment")
public class ADM_FragmentCliente extends Fragment {

    static SISTEMA SISTEMA = new SISTEMA();
    private UTILITARIOS Utilitarios = new UTILITARIOS();
    private ListView Lista_View;
    private ADM_AdaptadorUsuario Adaptador;
    private Dialog FormAregar;
    private Dialog FormBuscar;
    private String NormbreActividad = "CLIENTE";
    private String OA = "O";
    private static final String URL = SISTEMA.getURL() + "/Admin/Usuario";
    ArrayList<USUARIO> LISTA = new ArrayList<>();
    ArrayList<SELECTOR> SELECTORVENDEDOR = new ArrayList<>();
    //botones
    TextView TITULO;
    Spinner InputVendedor;
    EditText InputDni;
    EditText InputNombre;
    EditText InputApellido;
    EditText InputCorreo;
    EditText InputClave;
    Switch SwitchEstado;

    Button Btn_agregar;
    Button Btn_editar;
    Button Btn_modificar;
    Button Btn_eliminar;
    Button Btn_cerrar;
    //filtros
    Spinner InputColumna = null;
    EditText InputValor = null;
    int Filtros = 0;
    int Rol = 0;

    public ADM_FragmentCliente(int i) {
        this.Rol = i;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_global, container, false);
        Lista_View = (ListView) view.findViewById(R.id.ListView);
        TextView View = (TextView) view.findViewById(R.id.form_titulo);
        View.setText("NOMINA DE " + NormbreActividad);
        Lista_View.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DisplayInputDialog(i, view);
            }
        });

        final FloatingActionButton fab_agregar = (FloatingActionButton) view.findViewById(R.id.fab_agregar);
        fab_agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisplayInputDialog(-1, view);
            }
        });
        final FloatingActionButton fab_buscar = (FloatingActionButton) view.findViewById(R.id.fab_buscar);
        fab_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisplayBuscarDialog(-1, view);
            }
        });
        Filtros = 0;
        BuscarRegistros(view);

        return view;
    }

    private void CargarLista(View view) {
        //cargar etiquetas
        Adaptador = new ADM_AdaptadorUsuario(view.getContext(), LISTA, Lista_View, Rol);
        Lista_View.setAdapter(Adaptador);

    }

    private void DisplayBuscarDialog(final int pos, final View view) {
        FormBuscar = new Dialog(view.getContext());
        FormBuscar.setContentView(R.layout.busqueda_filtro);
        InputColumna = (Spinner) FormBuscar.findViewById(R.id.InputFiltro);
        InputValor = (EditText) FormBuscar.findViewById(R.id.InputValor);


        String[] letra = {"dni", "nombre", "apellido", "correo"};
        InputColumna.setAdapter(new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_item, letra));

        Button Btn_cerrar = (Button) FormBuscar.findViewById(R.id.Btn_cerrar);
        Button Btn_buscar = (Button) FormBuscar.findViewById(R.id.Btn_buscar);

        Btn_cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FormBuscar.dismiss();
                Filtros = 0;
            }
        });
        Btn_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Filtros = 1;
                BuscarRegistros(view);
            }
        });
        FormBuscar.show();
    }


    private void DisplayInputDialog(final int pos, final View view) {
        FormAregar = new Dialog(view.getContext());
        FormAregar.setTitle("NOMINA DE " + NormbreActividad);
        FormAregar.setContentView(R.layout.dialogo_admin_usuario);
        //cargar etiquetas
        TITULO = (TextView) FormAregar.findViewById(R.id.TextTitle);
        InputVendedor = (Spinner) FormAregar.findViewById(R.id.InputVendedor);
        //end selector

        TableRow GVendedor = (TableRow) FormAregar.findViewById(R.id.GVendedor);
        switch (Rol) {
            case 1:
                GVendedor.setVisibility(View.VISIBLE);
                ListasSpinner(view, URL, InputVendedor, pos);//CARGAR ESPINNER
                break;
            case 2:
                GVendedor.setVisibility(View.INVISIBLE);
                CARGARCOONENTES(pos, view);
                break;
        }


    }

    public void CARGARCOONENTES(final int pos, final View view) {
        InputDni = (EditText) FormAregar.findViewById(R.id.InputDni);
        InputNombre = (EditText) FormAregar.findViewById(R.id.InputNombre);
        InputApellido = (EditText) FormAregar.findViewById(R.id.InputApellido);
        InputCorreo = (EditText) FormAregar.findViewById(R.id.InputCorreo);
        InputVendedor.setEnabled(false);
        InputClave = (EditText) FormAregar.findViewById(R.id.InputClave);
        SwitchEstado = (Switch) FormAregar.findViewById(R.id.SwitchEstado);

        Btn_agregar = (Button) FormAregar.findViewById(R.id.Btn_agregar);
        Btn_editar = (Button) FormAregar.findViewById(R.id.Btn_editar);
        Btn_modificar = (Button) FormAregar.findViewById(R.id.Btn_modificar);
        Btn_eliminar = (Button) FormAregar.findViewById(R.id.Btn_eliminar);
        Btn_cerrar = (Button) FormAregar.findViewById(R.id.Btn_cerrar);

        Btn_modificar.setVisibility(View.INVISIBLE);
        switch (pos) {
            case -1:
                TITULO.setText("AGREGAR " + NormbreActividad);
                Btn_agregar.setVisibility(View.VISIBLE);
                Btn_editar.setVisibility(View.INVISIBLE);
                Btn_eliminar.setVisibility(View.INVISIBLE);
                HabilitarInput(true);
                InputClave.setText("Default");
                break;
            default:
                TITULO.setText("MODIFICAR " + NormbreActividad);
                Btn_agregar.setVisibility(View.INVISIBLE);
                Btn_editar.setVisibility(View.VISIBLE);
                Btn_eliminar.setVisibility(View.VISIBLE);
                //cargar elementos en etiquetas
                USUARIO Item = (USUARIO) Adaptador.getItem(pos);
                try {
                    int Posicion = Utilitarios.BuscarPosion(SELECTORVENDEDOR, Item.getId_Creador());
                    InputVendedor.setSelection(Posicion);
                } catch (Exception e) {
                }

                InputDni.setText(Item.getDni());
                InputNombre.setText(Item.getNombre());
                InputApellido.setText(Item.getApellido());
                InputCorreo.setText(Item.getCorreo());
                InputClave.setText(Item.getClave());
                SwitchEstado.setChecked(Item.getEstado());
                break;
        }


        Btn_agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                USUARIO obj = new USUARIO();
                obj.setDni(InputDni.getText().toString());
                obj.setNombre(InputNombre.getText().toString());
                obj.setApellido(InputApellido.getText().toString());
                obj.setCorreo(InputCorreo.getText().toString());
                obj.setClave(InputClave.getText().toString());
                obj.setEstado(SwitchEstado.isChecked());
                String Validar = Utilitarios.ValidarFormularioUsuario(obj);
                if (Validar == "OK") {
                    AgregarRegistros(view, obj);
                } else {
                    Toast.makeText(view.getContext(), "INGRESE " + Validar, Toast.LENGTH_SHORT).show();
                }


            }
        });

        Btn_editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Btn_modificar.setVisibility(View.VISIBLE);
                Btn_editar.setVisibility(View.INVISIBLE);
                HabilitarInput(true);
            }
        });

        Btn_modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HabilitarInput(false);
                USUARIO ItemActual = (USUARIO) Adaptador.getItem(pos);
                USUARIO obj = new USUARIO();
                obj.setID(ItemActual.getID());
                obj.setDni(InputDni.getText().toString());
                obj.setNombre(InputNombre.getText().toString());
                obj.setApellido(InputApellido.getText().toString());
                obj.setCorreo(InputCorreo.getText().toString());
                obj.setClave(InputClave.getText().toString());
                obj.setEstado(SwitchEstado.isChecked());
                String Validar = Utilitarios.ValidarFormularioUsuario(obj);
                if (Validar == "OK") {
                    Btn_editar.setVisibility(View.VISIBLE);
                    Btn_modificar.setVisibility(View.INVISIBLE);
                    ModificarRegistros(view, obj, pos);
                } else {
                    HabilitarInput(true);
                    Toast.makeText(view.getContext(), "INGRESE " + Validar, Toast.LENGTH_SHORT).show();
                }

            }
        });

        Btn_eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                USUARIO ItemActual = (USUARIO) Adaptador.getItem(pos);
                EliminarRegistros(view, Adaptador, ItemActual.getID(), pos);

            }
        });

        Btn_cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FormAregar.dismiss();
            }
        });
        FormAregar.show();
    }


    private void Limpiar() {
        InputDni.setText("");
        InputNombre.setText("");
        InputApellido.setText("");
        InputCorreo.setText("");
        InputClave.setText("");
        SwitchEstado.setChecked(true);
    }

    private void HabilitarInput(Boolean estado) {
        InputVendedor.setEnabled(estado);
        InputDni.setEnabled(estado);
        InputNombre.setEnabled(estado);
        InputApellido.setEnabled(estado);
        InputCorreo.setEnabled(estado);
        InputClave.setEnabled(estado);
        SwitchEstado.setEnabled(estado);
    }

    //MOSTRAR
    private void BuscarRegistros(final View view) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("BUSCANDO  ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    LISTA = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(response);
                    //  Integer id = (Integer) jsonObject.get("id");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject OB = jsonArray.getJSONObject(i);
                        int ID = OB.getInt("id");
                        String DNI = OB.getString("dni");
                        String NOMBRE = OB.getString("nombre");
                        String APELLIDO = OB.getString("apellido");
                        String CORREO = OB.getString("correo");
                        String CLAVE = OB.getString("clave");
                        String CREADOR = OB.getString("creador");
                        int ID_CREADOR = OB.getInt("id_creador");
                        int ESTADO_INT = OB.getInt("estado");
                        Boolean ESTADO = false;
                        if (ESTADO_INT == 1) {
                            ESTADO = true;
                        }
                        LISTA.add(new USUARIO(ID, 0, DNI, NOMBRE, APELLIDO, CORREO, CLAVE, CREADOR, ID_CREADOR, ESTADO));
                    }
                    CargarLista(view);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "show");
                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
                map.put("tipo_usuario", "3");//mostrar id  2 o vendedores
                map.put("Filtros", String.valueOf(Filtros));
                if (Filtros == 1) {
                    String Columna = InputColumna.getSelectedItem().toString();
                    map.put("Columna", Columna);
                    map.put("Valor", String.valueOf(InputValor.getText()));
                }
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }

    //AGREGAR
    private void AgregarRegistros(final View view, final USUARIO obj) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("CREANDO ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonArray = new JSONObject(response);
                    int ID = (int) jsonArray.get("id");
                    String CLAVE = jsonArray.get("clave").toString();
                    obj.setID(ID);
                    obj.setClave(CLAVE);

                    try {
                        int id_creador = SELECTORVENDEDOR.get(InputVendedor.getSelectedItemPosition()).getIndex();
                        String creador = InputVendedor.getSelectedItem().toString();
                        obj.setCreador(creador);
                        obj.setId_Creador(id_creador);
                    } catch (Exception e) {
                    }

                    if (ADM_AdaptadorUsuario.save(Adaptador, obj)) {
                        Toast.makeText(context, NormbreActividad + " GUARDAD" + OA, Toast.LENGTH_SHORT).show();
                        Limpiar();
                        FormAregar.dismiss();
                    } else {
                        //mandar a refrescar si no se guardo el registro de manera local
                        Filtros = 0;
                        BuscarRegistros(view);
                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "add");
                try {
                    int id_creador = SELECTORVENDEDOR.get(InputVendedor.getSelectedItemPosition()).getIndex();
                    map.put("id_creador", String.valueOf(id_creador));
                } catch (Exception e) {
                }

                map.put("dni", InputDni.getText().toString());
                map.put("nombre", InputNombre.getText().toString());
                map.put("apellido", InputApellido.getText().toString());
                map.put("correo", InputCorreo.getText().toString());
                map.put("clave", InputClave.getText().toString());
                String estado = "0";
                if (SwitchEstado.isChecked()) {
                    estado = "1";
                }
                map.put("estado", estado);
                map.put("tipo_usuario", "3");//mostrar id  2 o vendedores
                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));

                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }

    //MODIFICAR
    private void ModificarRegistros(final View view, final USUARIO obj, final int POSCION) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("ACTUALIZANDO ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonArray = new JSONObject(response);
                    String CLAVE = jsonArray.get("clave").toString();
                    obj.setClave(CLAVE);
                    try {
                        int id_creador = SELECTORVENDEDOR.get(InputVendedor.getSelectedItemPosition()).getIndex();
                        String creador = InputVendedor.getSelectedItem().toString();
                        obj.setCreador(creador);
                        obj.setId_Creador(id_creador);
                    } catch (Exception e) {
                    }
                    if (ADM_AdaptadorUsuario.update(Adaptador, POSCION, obj)) {
                        Toast.makeText(context, NormbreActividad + " MODIFICAD" + OA, Toast.LENGTH_SHORT).show();
                        Btn_modificar.setVisibility(View.INVISIBLE);
                        Btn_editar.setVisibility(View.VISIBLE);
                    } else {
                        //mandar a refrescar si no se guardo el registro de manera local
                        Filtros = 0;
                        BuscarRegistros(view);
                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "edi");
                int ID = obj.getID();
                try {
                    int id_creador = SELECTORVENDEDOR.get(InputVendedor.getSelectedItemPosition()).getIndex();
                    map.put("id_creador", String.valueOf(id_creador));
                } catch (Exception e) {
                }
                map.put("id", String.valueOf(ID));
                map.put("dni", InputDni.getText().toString());
                map.put("nombre", InputNombre.getText().toString());
                map.put("apellido", InputApellido.getText().toString());
                map.put("correo", InputCorreo.getText().toString());
                map.put("clave", InputClave.getText().toString());
                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));

                String estado = "0";
                if (SwitchEstado.isChecked()) {
                    estado = "1";
                }
                map.put("estado", estado);

                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }

    //MODIFICAR
    private void EliminarRegistros(final View view, final ADM_AdaptadorUsuario Adaptador, final int ID, final int POSCION) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("ELIMINADO..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (ADM_AdaptadorUsuario.delete(Adaptador, POSCION)) {
                    Toast.makeText(context, NormbreActividad + " ELIMINAD" + OA, Toast.LENGTH_SHORT).show();
                    FormAregar.dismiss();
                } else {
                    Toast.makeText(context, NormbreActividad + " NO SE ELIMIN" + OA, Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "del");
                map.put("id", String.valueOf(ID));
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }


    public void ListasSpinner(final View view, String URL, final Spinner inputVendedor, final int pos) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("BUSCANDO ..");
        //mostrar el progress
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONArray jsonArray = new JSONArray(response);
                    int T = jsonArray.length();
                    String[] valores = new String[T];
                    SELECTORVENDEDOR = new ArrayList<>();
                    if (T != 0) {
                        for (int i = 0; i < T; i++) {
                            JSONObject OB = jsonArray.getJSONObject(i);
                            String VALUE = OB.getString("value");
                            int INDEX = OB.getInt("index");
                            valores[i] = VALUE;
                            SELECTORVENDEDOR.add(new SELECTOR(INDEX, VALUE));
                        }

                        inputVendedor.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, valores));

                        CARGARCOONENTES(pos, view);
                    } else {
                        Toast.makeText(context, "No se existen vendedores", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "lista");
                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
                map.put("tipo_usuario", "2");//mostrar vendedores
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }


}
