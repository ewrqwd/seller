package com.example.micro_start.store.Controlador.Cliente;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.micro_start.store.Controlador.SharedPreferencesUsuario;
import com.example.micro_start.store.Controlador.UTILITARIOS;
import com.example.micro_start.store.Modelo.PRODUCTOS;
import com.example.micro_start.store.Modelo.SISTEMA;
import com.example.micro_start.store.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SuppressLint("ValidFragment")
public class CLI_FragmentProductos extends Fragment {
    static SISTEMA SISTEMA = new SISTEMA();
    private UTILITARIOS Utilitarios = new UTILITARIOS();
    private ListView Lista_View;
    private CLI_AdaptadorProductos Adaptador;
    private Dialog FormAregar;
    private Dialog FormBuscar;
    private String NormbreActividad = "PRODUCTO";
    private String OA = "A";
    private static final String URL = SISTEMA.getURL() + "/Clie/ItemsProducto";
    ArrayList<PRODUCTOS> LISTA = new ArrayList<>();

    // botones
    TextView TITULO;

    Button Btn_modificar;
    Button Btn_editar;
    Button Btn_cerrar;

    EditText InputCategoria;
    EditText InputNombre;
    EditText InputDescripcion;
    EditText InputCodigo;
    EditText InputPrecio;
    EditText InputItems;

    //filtros
    Spinner InputColumna = null;
    EditText InputValor = null;
    int Filtros = 0;
    int Rol = 0;
    int Pedido = 0;

    public CLI_FragmentProductos(int rol, int id_pedido) {
        this.Rol = rol;
        this.Pedido = id_pedido;
    }


    @SuppressLint("RestrictedApi")
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_global, container, false);
        Lista_View = (ListView) view.findViewById(R.id.ListView);
        TextView View = (TextView) view.findViewById(R.id.form_titulo);
        View.setText("NOMINA DE " + NormbreActividad);
        Lista_View.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DisplayInputDialog(i, view);
            }
        });

        final FloatingActionButton fab_agregar = (FloatingActionButton) view.findViewById(R.id.fab_agregar);
        fab_agregar.setVisibility(view.INVISIBLE);
        final FloatingActionButton fab_buscar = (FloatingActionButton) view.findViewById(R.id.fab_buscar);
        fab_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisplayBuscarDialog(-1, view);
            }
        });
        Filtros = 0;
        BuscarRegistros(view);

        return view;
    }

    private void CargarLista(View view) {
        //cargar etiquetas

        Adaptador = new CLI_AdaptadorProductos(view.getContext(), LISTA, Lista_View, Rol);
        Lista_View.setAdapter(Adaptador);

    }

    private void DisplayBuscarDialog(final int pos, final View view) {
        FormBuscar = new Dialog(view.getContext());
        FormBuscar.setContentView(R.layout.busqueda_filtro);
        InputColumna = (Spinner) FormBuscar.findViewById(R.id.InputFiltro);
        InputValor = (EditText) FormBuscar.findViewById(R.id.InputValor);
        String[] letra = {"categoria", "codigo", "nombre", "descripcion", "precio", "items"};

        InputColumna.setAdapter(new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_item, letra));

        Button Btn_cerrar = (Button) FormBuscar.findViewById(R.id.Btn_cerrar);
        Button Btn_buscar = (Button) FormBuscar.findViewById(R.id.Btn_buscar);

        Btn_cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FormBuscar.dismiss();
                Filtros = 0;
            }
        });
        Btn_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Filtros = 1;
                BuscarRegistros(view);
            }
        });
        FormBuscar.show();
    }

    private void DisplayInputDialog(final int pos, final View view) {
        FormAregar = new Dialog(view.getContext());
        FormAregar.setTitle("NOMINA DE " + NormbreActividad);
        FormAregar.setContentView(R.layout.dialogo_clie_producto);
        //cargar etiquetas
        TITULO = (TextView) FormAregar.findViewById(R.id.TextTitle);

        InputCategoria = (EditText) FormAregar.findViewById(R.id.InputCategoria);
        InputNombre = (EditText) FormAregar.findViewById(R.id.InputNombre);
        InputDescripcion = (EditText) FormAregar.findViewById(R.id.InputDescripcion);
        InputCodigo = (EditText) FormAregar.findViewById(R.id.InputCodigo);
        InputPrecio = (EditText) FormAregar.findViewById(R.id.InputPrecio);
        InputItems = (EditText) FormAregar.findViewById(R.id.InputItems);

        Btn_editar = (Button) FormAregar.findViewById(R.id.Btn_editar);
        Btn_modificar = (Button) FormAregar.findViewById(R.id.Btn_modificar);
        Btn_cerrar = (Button) FormAregar.findViewById(R.id.Btn_cerrar);


        Btn_modificar.setVisibility(View.INVISIBLE);
        TITULO.setText("MODIFICAR " + NormbreActividad);
        Btn_editar.setVisibility(View.VISIBLE);
        Btn_modificar.setVisibility(View.INVISIBLE);
        //cargar elementos en etiquetas
        final PRODUCTOS Item = (PRODUCTOS) Adaptador.getItem(pos);

        InputCategoria.setText(Item.getCategoria());
        InputNombre.setText(Item.getNombre());
        InputDescripcion.setText(Item.getDescripcion());
        InputCodigo.setText(Item.getCodigo());
        InputPrecio.setText(Item.getPrecio().toString());
        String Items = String.valueOf(Item.getId_Categoria());
        InputItems.setText(Items);


        Btn_editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Btn_modificar.setVisibility(View.VISIBLE);
                Btn_editar.setVisibility(View.INVISIBLE);
                HabilitarInput(true);
            }
        });

        Btn_modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HabilitarInput(false);
                PRODUCTOS ItemActual = (PRODUCTOS) Adaptador.getItem(pos);
                PRODUCTOS obj = new PRODUCTOS();
                obj.setId(ItemActual.getId());
                obj.setCategoria(ItemActual.getCategoria());
                obj.setNombre(InputNombre.getText().toString());
                obj.setDescripcion(InputDescripcion.getText().toString());
                obj.setCodigo(InputCodigo.getText().toString());
                obj.setPrecio(Double.parseDouble(InputPrecio.getText().toString()));
                obj.setEstado(ItemActual.getEstado());
                String c = InputItems.getText().toString();

                if (!c.equals("")) {

                    int Item = Integer.parseInt(c);
                    obj.setId_Categoria(Item);

                    Btn_editar.setVisibility(View.VISIBLE);
                    Btn_modificar.setVisibility(View.INVISIBLE);
                    ModificarRegistros(view, obj, pos);
                } else {
                    HabilitarInput(true);
                    Toast.makeText(view.getContext(), "INGRESE CANTIDAD DE ITEMS", Toast.LENGTH_SHORT).show();
                }

            }
        });


        Btn_cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FormAregar.dismiss();
            }
        });
        FormAregar.show();


    }


    private void HabilitarInput(Boolean estado) {

        InputItems.setEnabled(estado);
    }

    //MOSTRAR
    private void BuscarRegistros(final View view) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("BUSCANDO  ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    LISTA = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(response);
                    //  Integer id = (Integer) jsonObject.get("id");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject OB = jsonArray.getJSONObject(i);
                        int ID = OB.getInt("id_producto");
                        String NOMBRE = OB.getString("nombre");
                        String DESCRIPCION = OB.getString("descripcion");
                        String CODIGO = OB.getString("codigo");
                        Double PRECIO = Double.parseDouble(OB.getString("precio"));
                        String CATEGORIA = OB.getString("categoria");
                        int ITEM_COMPRADO = OB.getInt("items");

                        LISTA.add(new PRODUCTOS(ID, CODIGO, NOMBRE, DESCRIPCION, PRECIO, "", 0, CATEGORIA, ITEM_COMPRADO, true));
                    }
                    CargarLista(view);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "show");
                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
                map.put("id_pedido", String.valueOf(Pedido));
                map.put("tipo", String.valueOf(Rol));
                map.put("Filtros", String.valueOf(Filtros));
                if (Filtros == 1) {
                    String Columna = InputColumna.getSelectedItem().toString();
                    map.put("Columna", Columna);
                    map.put("Valor", String.valueOf(InputValor.getText()));
                }
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);

    }

    //MODIFICAR
    private void ModificarRegistros(final View view, final PRODUCTOS obj, final int POSCION) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("ACTUALIZANDO ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonArray = new JSONObject(response);

                    if (CLI_AdaptadorProductos.update(Adaptador, POSCION, obj)) {
                        Toast.makeText(context, NormbreActividad + " MODIFICAD" + OA, Toast.LENGTH_SHORT).show();
                        Btn_modificar.setVisibility(View.INVISIBLE);
                        Btn_editar.setVisibility(View.VISIBLE);
                    } else {
                        //mandar a refrescar si no se guardo el registro de manera local
                        Filtros = 0;
                        BuscarRegistros(view);
                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                PRODUCTOS ItemActual = (PRODUCTOS) Adaptador.getItem(POSCION);
                map.put("oper", "edi");
                map.put("id_pedido", String.valueOf(Pedido));
                map.put("id_producto", String.valueOf(ItemActual.getId()));
                map.put("items", InputItems.getText().toString());
                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);

    }


}
