package com.example.micro_start.store.Controlador.Administrador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.micro_start.store.R;

import java.util.ArrayList;

import com.example.micro_start.store.Modelo.PRODUCTOS;

public class ADM_AdaptadorProductos extends BaseAdapter {
    private static Context context;
    private static ArrayList<PRODUCTOS> ListaItem;
    private static ListView Lista_View;
    private static int Rol;

    public ADM_AdaptadorProductos(Context context, ArrayList<PRODUCTOS> listaItem, ListView lista_view, int Rol) {
        this.context = context;
        this.ListaItem = listaItem;
        this.Lista_View = lista_view;
        this.Rol = Rol;
    }


    @Override
    public int getCount() {//cantidad de contenido
        return ListaItem.size();
    }

    @Override
    public Object getItem(int position) {//devuelve el list item de la posicion
        return ListaItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PRODUCTOS Item = (PRODUCTOS) getItem(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.item_productos, null);
        TableRow GVendedor = (TableRow) convertView.findViewById(R.id.GVendedor);

        TextView Vendedor = (TextView) convertView.findViewById(R.id.ItemVendedor);
        TextView Categoria = (TextView) convertView.findViewById(R.id.ItemCategoria);
        TextView Codigo = (TextView) convertView.findViewById(R.id.ItemCodigo);
        TextView Nombre = (TextView) convertView.findViewById(R.id.ItemNombre);
        TextView Descripcion = (TextView) convertView.findViewById(R.id.ItemDescripcion);
        TextView Precio = (TextView) convertView.findViewById(R.id.ItemPrecio);
        TextView Estado = (TextView) convertView.findViewById(R.id.ItemEstado);
        Vendedor.setText(Item.getVendedor());
        Categoria.setText(Item.getCategoria());
        Codigo.setText(Item.getCodigo());
        Nombre.setText(Item.getNombre());
        Descripcion.setText(Item.getDescripcion());
        Precio.setText(Item.getPrecio().toString());
        String estado = "DESHABILITADO";
        if (Item.getEstado()) {
            estado = "HABILITADO";
        }
        Estado.setText(estado);

        switch (Rol) {
            case 1:
                GVendedor.setVisibility(View.VISIBLE);
                break;

            default:
                GVendedor.setVisibility(View.INVISIBLE);
                break;
        }
        return convertView;
    }


    public static Boolean delete(ADM_AdaptadorProductos Adaptador, int position) {
        try {
            ListaItem.remove(position);
            Adaptador = new ADM_AdaptadorProductos(context, ListaItem, Lista_View, Rol);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }

    public static Boolean save(ADM_AdaptadorProductos Adaptador, PRODUCTOS OBJ) {

        try {
            ListaItem.add(OBJ);
            Adaptador = new ADM_AdaptadorProductos(context, ListaItem, Lista_View, Rol);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }

    public static Boolean update(ADM_AdaptadorProductos Adaptador, int position, PRODUCTOS OBJ) {

        try {

            ListaItem.remove(position);
            ListaItem.add(position, OBJ);
            Adaptador = new ADM_AdaptadorProductos(context, ListaItem, Lista_View, Rol);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }


}
