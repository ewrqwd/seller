package com.example.micro_start.store.Modelo;

public class PRODUCTOS {
    private int Id;

    private String Codigo;
    private String Nombre;
    private String Descripcion;
    private Double Precio;
    private String Vendedor;
    private int Id_Vendedor;
    private String Categoria;
    private int Id_Categoria;
    private Boolean Estado;

    public PRODUCTOS() {
    }

    public PRODUCTOS(int id, String codigo, String nombre, String descripcion, Double precio, String vendedor, int id_Vendedor, String categoria, int id_Categoria, Boolean estado) {
        Id = id;
        Codigo = codigo;
        Nombre = nombre;
        Descripcion = descripcion;
        Precio = precio;
        Vendedor = vendedor;
        Id_Vendedor = id_Vendedor;
        Categoria = categoria;
        Id_Categoria = id_Categoria;
        Estado = estado;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public Double getPrecio() {
        return Precio;
    }

    public void setPrecio(Double precio) {
        Precio = precio;
    }

    public String getVendedor() {
        return Vendedor;
    }

    public void setVendedor(String vendedor) {
        Vendedor = vendedor;
    }

    public int getId_Vendedor() {
        return Id_Vendedor;
    }

    public void setId_Vendedor(int id_Vendedor) {
        Id_Vendedor = id_Vendedor;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String categoria) {
        Categoria = categoria;
    }

    public int getId_Categoria() {
        return Id_Categoria;
    }

    public void setId_Categoria(int id_Categoria) {
        Id_Categoria = id_Categoria;
    }

    public Boolean getEstado() {
        return Estado;
    }

    public void setEstado(Boolean estado) {
        Estado = estado;
    }
}
