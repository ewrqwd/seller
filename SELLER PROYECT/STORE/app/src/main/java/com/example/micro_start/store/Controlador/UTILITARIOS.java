package com.example.micro_start.store.Controlador;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.micro_start.store.Modelo.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class UTILITARIOS {
   public  ArrayList<SELECTOR>  LISTA  ;


    //gestor de validaciones
    public String ValidarFormularioUsuario(USUARIO C) {
        String MSJ = "OK";
        if (C.getNombre().length() == 0) {
            MSJ = "Nombre";
        } else if (C.getApellido().length() == 0) {
            MSJ = "Apellido";
        } else if (C.getCorreo().length() == 0) {
            MSJ = "Correo";
        } else if (C.getClave().length() == 0) {
            MSJ = "Clave";
        }
        return MSJ;
    }

    //gestor de validaciones
    public String ValidarFormularioCategoria(CATEGORIA C) {
        String MSJ = "OK";
        if (C.getNombre().length() == 0) {
            MSJ = "Nombre";
        } else if (C.getDescripcion().length() == 0) {
            MSJ = "Descripcion";
        }
        return MSJ;
    }

    //errrores
    public void MensajeError(Context context, ProgressDialog progressDialog, VolleyError error) {
        progressDialog.dismiss();
        Log.e("ERROR", error.toString());
        Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
    }

    public String ValidarFormularioProducto(PRODUCTOS C) {
        String MSJ = "OK";
        if (C.getCodigo().length() == 0) {
            MSJ = "Codigo";
        } else if (C.getNombre().length() == 0) {
            MSJ = "Nombre";
        } else if (C.getDescripcion().length() == 0) {
            MSJ = "Decripcion";
        }
       else if (C.getPrecio() == null) {
            MSJ = "Precio";
        }
        return MSJ;
    }
    public String ValidarFormularioVenta(PEDIDO_CLI C) {
        String MSJ = "OK";
        if (C.getCodigo().length() == 0) {
            MSJ = "Codigo";
        } else if (C.getDescripcion().length() == 0) {
            MSJ = "Descripcion";
        }
        else if (C.getFecha() == null) {
            MSJ = "Fecha";
        }
        else if (C.getTotal() == -1) {
            MSJ = "Total";
        }
        return MSJ;
    }
    public void  ListasSpinner(final View view, String URL, final Map<String, String> map, final Spinner inputVendedor) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("BUSCANDO LISTAS ..");
        //mostrar el progress
        progressDialog.show();



        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONArray jsonArray = new JSONArray(response);
                    int T=jsonArray.length();
                    String []  valores = new String [T];

                   for (int i = 0; i < T; i++) {
                        JSONObject OB = jsonArray.getJSONObject(i);
                        String VALUE= OB.getString("value");
                        int INDEX= OB.getInt("index");
                       valores[i] = VALUE;
                       LISTA.add(new SELECTOR(INDEX, VALUE));
                    }

                    inputVendedor.setAdapter(new ArrayAdapter<String>( context , android.R.layout.simple_spinner_item, valores ));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                 MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }


    public int BuscarPosion(ArrayList<SELECTOR> selectorvendedor, int index) {
        int post=0;
        for (int i = 0; i < selectorvendedor.size(); i++) {

           if ( selectorvendedor.get(i).getIndex()== index){
               return i;//devuelve la pisicion
           }
        }

        return post;//  usar el primer registro
    }


}