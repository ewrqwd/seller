package com.example.micro_start.store.Controlador.Vendedor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.micro_start.store.Modelo.PEDIDO_CLI;
import com.example.micro_start.store.Modelo.PEDIDO_VEND;
import com.example.micro_start.store.R;

import java.util.ArrayList;

public class VEN_AdaptadorPedido extends BaseAdapter {
    private static Context context;
    private static ArrayList<PEDIDO_VEND> ListaItem;
    private static ListView Lista_View;
    private static int Rol;

    public VEN_AdaptadorPedido(Context context, ArrayList<PEDIDO_VEND> listaItem, ListView lista_view, int Rol) {
        this.context = context;
        this.ListaItem = listaItem;
        this.Lista_View = lista_view;
        this.Rol = Rol;
    }


    @Override
    public int getCount() {//cantidad de contenido
        return ListaItem.size();
    }

    @Override
    public Object getItem(int position) {//devuelve el list item de la posicion
        return ListaItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PEDIDO_VEND Item = (PEDIDO_VEND) getItem(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.item_ven_pedido, null);

        TextView DNI= (TextView) convertView.findViewById(R.id.ItemDni);
        TextView NOMBE= (TextView) convertView.findViewById(R.id.ItemNombre);
        TextView APELLIDO= (TextView) convertView.findViewById(R.id.ItemApellido);
        TextView Codigo = (TextView) convertView.findViewById(R.id.ItemCodigo);
        TextView Descripcion = (TextView) convertView.findViewById(R.id.ItemDescripcion);
        TextView Fecha = (TextView) convertView.findViewById(R.id.ItemFecha);
        TextView Total = (TextView) convertView.findViewById(R.id.ItemTotal);

        DNI.setText(Item.getDni());
        NOMBE.setText(Item.getNombre());
        APELLIDO.setText(Item.getApellido());
        Codigo.setText(Item.getCodigo());
        Descripcion.setText(Item.getDescripcion());
        Fecha.setText(Item.getFecha());
        Total.setText(Item.getTotal().toString());

        return convertView;
    }


    public static Boolean delete(VEN_AdaptadorPedido Adaptador, int position) {
        try {
            ListaItem.remove(position);
            Adaptador = new VEN_AdaptadorPedido(context, ListaItem, Lista_View, Rol);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }

    public static Boolean save(VEN_AdaptadorPedido Adaptador, PEDIDO_VEND OBJ) {

        try {
            ListaItem.add(OBJ);
            Adaptador = new VEN_AdaptadorPedido(context, ListaItem, Lista_View, Rol);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }

    public static Boolean update(VEN_AdaptadorPedido Adaptador, int position, PEDIDO_VEND OBJ) {

        try {

            ListaItem.remove(position);
            ListaItem.add(position, OBJ);
            Adaptador = new VEN_AdaptadorPedido(context, ListaItem, Lista_View, Rol);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }


}
