package com.example.micro_start.store.Modelo;

public class USUARIO {
    private int ID;
    private int Id_Rol;
    private String Dni;
    private String Nombre;
    private String Apellido;
    private String Correo;
    private String Clave;
    private String Creador;
    private int Id_Creador;
    private Boolean Estado;

    public USUARIO() {
    }

    public USUARIO(int ID, int id_Rol, String dni, String nombre, String apellido, String correo, String clave, String creador, int id_Creador, Boolean estado) {
        this.ID = ID;
        Id_Rol = id_Rol;
        Dni = dni;
        Nombre = nombre;
        Apellido = apellido;
        Correo = correo;
        Clave = clave;
        Creador = creador;
        Id_Creador = id_Creador;
        Estado = estado;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getId_Rol() {
        return Id_Rol;
    }

    public void setId_Rol(int id_Rol) {
        Id_Rol = id_Rol;
    }

    public String getDni() {
        return Dni;
    }

    public void setDni(String dni) {
        Dni = dni;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public String getClave() {
        return Clave;
    }

    public void setClave(String clave) {
        Clave = clave;
    }

    public String getCreador() {
        return Creador;
    }

    public void setCreador(String creador) {
        Creador = creador;
    }

    public int getId_Creador() {
        return Id_Creador;
    }

    public void setId_Creador(int id_Creador) {
        Id_Creador = id_Creador;
    }

    public Boolean getEstado() {
        return Estado;
    }

    public void setEstado(Boolean estado) {
        Estado = estado;
    }
}
