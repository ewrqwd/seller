package com.example.micro_start.store.Controlador.Global;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.micro_start.store.Modelo.PEDIDO_VEND;
import com.example.micro_start.store.R;

import java.util.ArrayList;

public class GLO_AdaptadorPago extends BaseAdapter {
    private static Context context;
    private static ArrayList<PEDIDO_VEND> ListaItem;
    private static ListView Lista_View;
    private static int Rol;

    public GLO_AdaptadorPago(Context context, ArrayList<PEDIDO_VEND> listaItem, ListView lista_view, int Rol) {
        this.context = context;
        this.ListaItem = listaItem;
        this.Lista_View = lista_view;
        this.Rol = Rol;
    }


    @Override
    public int getCount() {//cantidad de contenido
        return ListaItem.size();
    }

    @Override
    public Object getItem(int position) {//devuelve el list item de la posicion
        return ListaItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PEDIDO_VEND Item = (PEDIDO_VEND) getItem(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.item_glo_pedido, null);

        TextView DNI= (TextView) convertView.findViewById(R.id.ItemDni);
        TextView NOMBE= (TextView) convertView.findViewById(R.id.ItemNombre);
        TextView APELLIDO= (TextView) convertView.findViewById(R.id.ItemApellido);
        TextView Codigo = (TextView) convertView.findViewById(R.id.ItemCodigo);
        TextView Descripcion = (TextView) convertView.findViewById(R.id.ItemDescripcion);
        TextView Fecha = (TextView) convertView.findViewById(R.id.ItemFecha);
        TextView Deuda = (TextView) convertView.findViewById(R.id.ItemDeuda);
        TextView Pago = (TextView) convertView.findViewById(R.id.ItemPago);
        TextView Total = (TextView) convertView.findViewById(R.id.ItemTotal);

        DNI.setText(Item.getDni());
        NOMBE.setText(Item.getNombre());
        APELLIDO.setText(Item.getApellido());
        Codigo.setText(Item.getCodigo());
        Descripcion.setText(Item.getDescripcion());
        Fecha.setText(Item.getFecha());
        Deuda.setText(Item.getDeuda().toString());
        Pago.setText(Item.getPago().toString());
        Total.setText(Item.getTotal().toString());

        return convertView;
    }


    public static Boolean delete(GLO_AdaptadorPago Adaptador, int position) {
        try {
            ListaItem.remove(position);
            Adaptador = new GLO_AdaptadorPago(context, ListaItem, Lista_View, Rol);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }

    public static Boolean save(GLO_AdaptadorPago Adaptador, PEDIDO_VEND OBJ) {

        try {
            ListaItem.add(OBJ);
            Adaptador = new GLO_AdaptadorPago(context, ListaItem, Lista_View, Rol);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }

    public static Boolean update(GLO_AdaptadorPago Adaptador, int position, PEDIDO_VEND OBJ) {

        try {

            ListaItem.remove(position);
            ListaItem.add(position, OBJ);
            Adaptador = new GLO_AdaptadorPago(context, ListaItem, Lista_View, Rol);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }


}
