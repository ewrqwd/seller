package com.example.micro_start.store.Modelo;

import java.util.Date;

public class PEDIDO_CLI {
    private  int Id;
    private  String Codigo;
    private  String Descripcion;
    private String  Fecha;
    private Double Total;
    private  Boolean Estado;

    public PEDIDO_CLI() {
    }

    public PEDIDO_CLI(int id, String codigo, String descripcion, String fecha, Double total, Boolean estado) {
        Id = id;
        Codigo = codigo;
        Descripcion = descripcion;
        Fecha = fecha;
        Total = total;
        Estado = estado;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public Double getTotal() {
        return Total;
    }

    public void setTotal(Double total) {
        Total = total;
    }

    public Boolean getEstado() {
        return Estado;
    }

    public void setEstado(Boolean estado) {
        Estado = estado;
    }
}
