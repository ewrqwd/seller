package com.example.micro_start.store.Modelo;

public class PEDIDO_VEND {
    private  int Id;
    private  String Dni;
    private  String Nombre;
    private  String Apellido;
    private  String Codigo;
    private  String Descripcion;
    private String  Fecha;
    private Double Deuda;
    private Double Pago;
    private Double Total;
    private  Boolean Estado;

    public PEDIDO_VEND(int id, String dni, String nombre, String apellido, String codigo, String descripcion, String fecha, Double deuda, Double pago, Double total, Boolean estado) {
        Id = id;
        Dni = dni;
        Nombre = nombre;
        Apellido = apellido;
        Codigo = codigo;
        Descripcion = descripcion;
        Fecha = fecha;
        Deuda = deuda;
        Pago = pago;
        Total = total;
        Estado = estado;
    }

    public PEDIDO_VEND() {
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDni() {
        return Dni;
    }

    public void setDni(String dni) {
        Dni = dni;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }



    public Double getDeuda() {
        return Deuda;
    }

    public void setDeuda(Double deuda) {
        Deuda = deuda;
    }

    public Double getPago() {
        return Pago;
    }

    public void setPago(Double pago) {
        Pago = pago;
    }

    public Double getTotal() {
        return Total;
    }

    public void setTotal(Double total) {
        Total = total;
    }

    public Boolean getEstado() {
        return Estado;
    }

    public void setEstado(Boolean estado) {
        Estado = estado;
    }
}
