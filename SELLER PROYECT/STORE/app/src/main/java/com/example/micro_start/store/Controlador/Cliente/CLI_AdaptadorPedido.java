package com.example.micro_start.store.Controlador.Cliente;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.micro_start.store.Modelo.PEDIDO_CLI;
import com.example.micro_start.store.R;

import java.util.ArrayList;

public class CLI_AdaptadorPedido extends BaseAdapter {
    private static Context context;
    private static ArrayList<PEDIDO_CLI> ListaItem;
    private static ListView Lista_View;
    private static int Rol;

    public CLI_AdaptadorPedido(Context context, ArrayList<PEDIDO_CLI> listaItem, ListView lista_view, int Rol) {
        this.context = context;
        this.ListaItem = listaItem;
        this.Lista_View = lista_view;
        this.Rol = Rol;
    }


    @Override
    public int getCount() {//cantidad de contenido
        return ListaItem.size();
    }

    @Override
    public Object getItem(int position) {//devuelve el list item de la posicion
        return ListaItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PEDIDO_CLI Item = (PEDIDO_CLI) getItem(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.item_cli_pedido, null);

        TextView Codigo = (TextView) convertView.findViewById(R.id.ItemCodigo);
        TextView Descripcion = (TextView) convertView.findViewById(R.id.ItemDescripcion);
        TextView Fecha = (TextView) convertView.findViewById(R.id.ItemFecha);
        TextView Estado = (TextView) convertView.findViewById(R.id.ItemEstado);
        TextView Total = (TextView) convertView.findViewById(R.id.ItemTotal);


        Codigo.setText(Item.getCodigo());
        Descripcion.setText(Item.getDescripcion());
        Fecha.setText(Item.getFecha());
        Total.setText(Item.getTotal().toString());

        String estado = "DESHABILITADO";
        if (Item.getEstado()) {
            estado = "HABILITADO";
        }
        Estado.setText(estado);
        return convertView;
    }


    public static Boolean delete(CLI_AdaptadorPedido Adaptador, int position) {
        try {
            ListaItem.remove(position);
            Adaptador = new CLI_AdaptadorPedido(context, ListaItem, Lista_View, Rol);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }

    public static Boolean save(CLI_AdaptadorPedido Adaptador, PEDIDO_CLI OBJ) {

        try {
            ListaItem.add(OBJ);
            Adaptador = new CLI_AdaptadorPedido(context, ListaItem, Lista_View, Rol);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }

    public static Boolean update(CLI_AdaptadorPedido Adaptador, int position, PEDIDO_CLI OBJ) {

        try {

            ListaItem.remove(position);
            ListaItem.add(position, OBJ);
            Adaptador = new CLI_AdaptadorPedido(context, ListaItem, Lista_View, Rol);
            Lista_View.setAdapter(Adaptador);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
    }


}
