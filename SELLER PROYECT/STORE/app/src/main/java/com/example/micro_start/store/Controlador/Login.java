package com.example.micro_start.store.Controlador;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.micro_start.store.MainActivity;
import com.example.micro_start.store.Modelo.*;
import com.example.micro_start.store.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity implements View.OnClickListener {
    static SISTEMA SISTEMA = new SISTEMA();
    private EditText correo, clave;
    private Button login, register;
    private static final String URL_LOGIN = SISTEMA.getURL() + "/login";
    private SharedPreferencesUsuario sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedPref = new SharedPreferencesUsuario(this);
        if (sharedPref.ValidarLogiado()) {
            finish();
            startActivity(new Intent(Login.this, MainActivity.class));

        } else {
            correo = (EditText) findViewById(R.id.TextCorreo);
            clave = (EditText) findViewById(R.id.TextClave);
            login = (Button) findViewById(R.id.BTNLoginL);
            login.setOnClickListener(this);
            //  register.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.BTNLoginL:

                if (correo.getText().toString().isEmpty()) {
                    correo.setError("CAMPO NECESARIO");
                } else if (clave.getText().toString().isEmpty()) {
                    clave.setError("CAMPO NECESARIO");
                } else {
                    ISesion();
                }


        }
    }

    private void ISesion() {
        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(this);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("ESPERE....");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL_LOGIN
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                switch (response) {
                    case "0":
                        Toast.makeText(Login.this, "EL USUARIO NO EXISTE", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                         //   Integer id = (Integer) jsonObject.get("id");
                            SharedPreferencesUsuario.getInstance(getApplicationContext()).GuardarUsuarioLogiado(jsonObject);
                            finish();
                            startActivity(new Intent(Login.this, MainActivity.class));
                      } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("ERROR", error.toString());
                Toast.makeText(Login.this, error.toString(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("correo", correo.getText().toString().trim());
                map.put("clave", clave.getText().toString().trim());
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringRequest);
    }
}

