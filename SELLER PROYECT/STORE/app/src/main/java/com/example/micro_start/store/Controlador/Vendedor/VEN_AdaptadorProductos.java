//package com.example.micro_start.store.Controlador.Vendedor;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import com.example.micro_start.store.Modelo.PRODUCTOS;
//import com.example.micro_start.store.R;
//
//import java.util.ArrayList;
//
//public class VEN_AdaptadorProductos extends BaseAdapter {
//    private static Context context;
//    private static ArrayList<PRODUCTOS> ListaItem;
//    private static ListView Lista_View;
//    private static int Rol;
//
//    public VEN_AdaptadorProductos(Context context, ArrayList<PRODUCTOS> listaItem, ListView lista_view, int Rol) {
//        this.context = context;
//        this.ListaItem = listaItem;
//        this.Lista_View = lista_view;
//        this.Rol = Rol;
//    }
//
//
//    @Override
//    public int getCount() {//cantidad de contenido
//        return ListaItem.size();
//    }
//
//    @Override
//    public Object getItem(int position) {//devuelve el list item de la posicion
//        return ListaItem.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return 0;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        PRODUCTOS Item = (PRODUCTOS) getItem(position);
//        convertView = LayoutInflater.from(context).inflate(R.layout.item_cli_productos, null);
//
//
//        TextView Categoria = (TextView) convertView.findViewById(R.id.ItemCategoria);
//        TextView Codigo = (TextView) convertView.findViewById(R.id.ItemCodigo);
//        TextView Nombre = (TextView) convertView.findViewById(R.id.ItemNombre);
//        TextView Descripcion = (TextView) convertView.findViewById(R.id.ItemDescripcion);
//        TextView Precio = (TextView) convertView.findViewById(R.id.ItemPrecio);
//        TextView ItemItems = (TextView) convertView.findViewById(R.id.ItemItems);
//        TextView ItemTotal = (TextView) convertView.findViewById(R.id.ItemTotal);
//
//        Categoria.setText(Item.getCategoria());
//        Codigo.setText(Item.getCodigo());
//        Nombre.setText(Item.getNombre());
//        Descripcion.setText(Item.getDescripcion());
//        Double PRECIO = Item.getPrecio();
//        Precio.setText(PRECIO.toString());
//        int CANTIDA = Item.getId_Categoria();
//        ItemItems.setText(String.valueOf(CANTIDA));
//        Double Total = 0.0;
//        try {
//            Total = CANTIDA * PRECIO;
//        } catch (Exception e) {
//
//        }
//        ItemTotal.setText(Total.toString());
//
//
//
//
//
//        return convertView;
//    }
//
//
//    public static Boolean delete(VEN_AdaptadorProductos Adaptador, int position) {
//        try {
//            ListaItem.remove(position);
//            Adaptador = new VEN_AdaptadorProductos(context, ListaItem, Lista_View, Rol);
//            Lista_View.setAdapter(Adaptador);
//            return true;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//
//        }
//    }
//
//    public static Boolean save(VEN_AdaptadorProductos Adaptador, PRODUCTOS OBJ) {
//
//        try {
//            ListaItem.add(OBJ);
//            Adaptador = new VEN_AdaptadorProductos(context, ListaItem, Lista_View, Rol);
//            Lista_View.setAdapter(Adaptador);
//            return true;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//
//        }
//    }
//
//    public static Boolean update(VEN_AdaptadorProductos Adaptador, int position, PRODUCTOS OBJ) {
//
//        try {
//
//            ListaItem.remove(position);
//            ListaItem.add(position, OBJ);
//            Adaptador = new VEN_AdaptadorProductos(context, ListaItem, Lista_View, Rol);
//            Lista_View.setAdapter(Adaptador);
//            return true;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//
//        }
//    }
//
//
//}
