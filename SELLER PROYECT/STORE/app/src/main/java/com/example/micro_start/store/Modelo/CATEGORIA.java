package com.example.micro_start.store.Modelo;

public class CATEGORIA {
    private  int Id;
    private  String Nombre;
    private  String Descripcion;
    private  String Codigo;
    private String Creador;
    private int Id_Creador;
    private  Boolean Estado;

    public CATEGORIA(int id, String nombre, String descripcion, String codigo, String creador, int id_Creador, Boolean estado) {
        Id = id;
        Nombre = nombre;
        Descripcion = descripcion;
        Codigo = codigo;
        Creador = creador;
        Id_Creador = id_Creador;
        Estado = estado;
    }

    public CATEGORIA() {
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getCreador() {
        return Creador;
    }

    public void setCreador(String creador) {
        Creador = creador;
    }

    public int getId_Creador() {
        return Id_Creador;
    }

    public void setId_Creador(int id_Creador) {
        Id_Creador = id_Creador;
    }

    public Boolean getEstado() {
        return Estado;
    }

    public void setEstado(Boolean estado) {
        Estado = estado;
    }
}
