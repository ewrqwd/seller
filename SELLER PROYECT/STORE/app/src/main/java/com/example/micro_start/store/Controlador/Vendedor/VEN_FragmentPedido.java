package com.example.micro_start.store.Controlador.Vendedor;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.micro_start.store.Controlador.Cliente.CLI_FragmentProductos;
import com.example.micro_start.store.Controlador.SharedPreferencesUsuario;
import com.example.micro_start.store.Controlador.UTILITARIOS;
import com.example.micro_start.store.Modelo.PEDIDO_CLI;
import com.example.micro_start.store.Modelo.PEDIDO_VEND;
import com.example.micro_start.store.Modelo.SISTEMA;
import com.example.micro_start.store.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@SuppressLint("ValidFragment")
public class VEN_FragmentPedido extends Fragment {
    static SISTEMA SISTEMA = new SISTEMA();
    private UTILITARIOS Utilitarios = new UTILITARIOS();
    private ListView Lista_View;
    private VEN_AdaptadorPedido Adaptador;
    private Dialog FormAregar;
    private Dialog FormBuscar;
    private String NormbreActividad = "PEDIDO";
    private String OA = "O";
    private static final String URL = SISTEMA.getURL() + "/Vend/ItemsPedido";
    ArrayList<PEDIDO_VEND> LISTA = new ArrayList<>();
    //botones
    TextView TITULO;
    EditText InputDni;
    EditText InputNombre;
    EditText InputApellido;
    EditText InputCodigo;
    EditText InputDescripcion;
    EditText InputFecha;
    EditText InputTotal;
    EditText InputPago;
    EditText InputDeuda;


    Button Btn_editar;
    Button Btn_modificar;
    Button Btn_eliminar;
    Button Btn_producto;
    Button Btn_cerrar;
    //filtros
    Spinner InputColumna = null;
    EditText InputValor = null;
    int Filtros = 0;
    int Rol = 0;

    public VEN_FragmentPedido(int i) {
        this.Rol = i;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_global, container, false);
        Lista_View = (ListView) view.findViewById(R.id.ListView);
        TextView View = (TextView) view.findViewById(R.id.form_titulo);
        View.setText("NOMINA DE " + NormbreActividad);
        Lista_View.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DisplayInputDialog(i, view);
            }
        });

        final FloatingActionButton fab_agregar = (FloatingActionButton) view.findViewById(R.id.fab_agregar);
        fab_agregar.setVisibility(view.INVISIBLE);
        final FloatingActionButton fab_buscar = (FloatingActionButton) view.findViewById(R.id.fab_buscar);
        fab_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisplayBuscarDialog(-1, view);
            }
        });
        Filtros = 0;
        BuscarRegistros(view);

        return view;
    }

    private void CargarLista(View view) {
        //cargar etiquetas

        Adaptador = new VEN_AdaptadorPedido(view.getContext(), LISTA, Lista_View, Rol);
        Lista_View.setAdapter(Adaptador);

    }

    private void DisplayBuscarDialog(final int pos, final View view) {
        FormBuscar = new Dialog(view.getContext());
        FormBuscar.setContentView(R.layout.busqueda_filtro);
        InputColumna = (Spinner) FormBuscar.findViewById(R.id.InputFiltro);
        InputValor = (EditText) FormBuscar.findViewById(R.id.InputValor);


        String[] letra = {"dni", "nombre", "apellido", "codigo", "decripcion", "total"};
        InputColumna.setAdapter(new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_item, letra));

        Button Btn_cerrar = (Button) FormBuscar.findViewById(R.id.Btn_cerrar);
        Button Btn_buscar = (Button) FormBuscar.findViewById(R.id.Btn_buscar);

        Btn_cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FormBuscar.dismiss();
                Filtros = 0;
            }
        });
        Btn_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Filtros = 1;
                BuscarRegistros(view);
            }
        });


        FormBuscar.show();
    }

    private void DisplayInputDialog(final int pos, final View view) {
        FormAregar = new Dialog(view.getContext());
        FormAregar.setTitle("NOMINA DE " + NormbreActividad);
        FormAregar.setContentView(R.layout.dialogo_ven_pedido);
        //cargar etiquetas
        TITULO = (TextView) FormAregar.findViewById(R.id.TextTitle);

        CARGARCOONENTES(pos, view);


    }


    public void CARGARCOONENTES(final int pos, final View view) {
        InputDni = (EditText) FormAregar.findViewById(R.id.InputDni);
        InputNombre = (EditText) FormAregar.findViewById(R.id.InputNombre);
        InputApellido = (EditText) FormAregar.findViewById(R.id.InputApellido);
        InputCodigo = (EditText) FormAregar.findViewById(R.id.InputCodigo);
        InputDescripcion = (EditText) FormAregar.findViewById(R.id.InputDescripcion);
        InputFecha = (EditText) FormAregar.findViewById(R.id.InputFecha);
        InputTotal = (EditText) FormAregar.findViewById(R.id.InputTotal);
        InputPago = (EditText) FormAregar.findViewById(R.id.InputPago);
        InputDeuda = (EditText) FormAregar.findViewById(R.id.InputDeuda);

        Btn_editar = (Button) FormAregar.findViewById(R.id.Btn_editar);
        Btn_modificar = (Button) FormAregar.findViewById(R.id.Btn_modificar);
        Btn_eliminar = (Button) FormAregar.findViewById(R.id.Btn_eliminar);
        Btn_producto = (Button) FormAregar.findViewById(R.id.Btn_product);
        Btn_cerrar = (Button) FormAregar.findViewById(R.id.Btn_cerrar);

        Btn_modificar.setVisibility(View.INVISIBLE);

        TITULO.setText("MODIFICAR " + NormbreActividad);
        Btn_editar.setVisibility(View.VISIBLE);
        Btn_eliminar.setVisibility(View.VISIBLE);
        //cargar elementos en etiquetas
        final PEDIDO_VEND Item = (PEDIDO_VEND) Adaptador.getItem(pos);
        InputDni.setText(Item.getDni());
        InputNombre.setText(Item.getNombre());
        InputApellido.setText(Item.getApellido());
        InputCodigo.setText(Item.getCodigo());
        InputDescripcion.setText(Item.getDescripcion());
        InputFecha.setText(Item.getFecha());
        InputTotal.setText(Item.getTotal().toString());
        InputPago.setText(Item.getPago().toString());
        InputDeuda.setText(Item.getTotal().toString());


        InputPago.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {


                double Deuda = Item.getTotal();
                try {
                    Deuda = Deuda - Double.parseDouble(InputPago.getText().toString());
                } catch (Exception e) {
                }

                InputDeuda.setText(String.valueOf(Deuda));


                return false;
            }
        });
        Btn_editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Btn_modificar.setVisibility(View.VISIBLE);
                Btn_editar.setVisibility(View.INVISIBLE);

                HabilitarInput(true);
            }
        });

        Btn_modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String c = InputPago.getText().toString();
                if (!c.equals("")) {

                    HabilitarInput(false);
                    PEDIDO_VEND ItemActual = (PEDIDO_VEND) Adaptador.getItem(pos);

                    Double T = Double.parseDouble(ItemActual .getTotal().toString());
                    Double P = Double.parseDouble(InputPago.getText().toString());

                    if (T >= P) {
                        Btn_editar.setVisibility(View.VISIBLE);
                        Btn_modificar.setVisibility(View.INVISIBLE);
                        ModificarRegistros(view, ItemActual, pos);
                    } else {
                        HabilitarInput(true);
                        Toast.makeText(view.getContext(), "EL PAGO DEBE SER MENOR O IGUAL AL TOTALO DEUDA ", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(view.getContext(), "INGRSE UN VALOR DE PAGO O ABONO ", Toast.LENGTH_LONG).show();

                }
            }
        });

        Btn_eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PEDIDO_VEND ItemActual = (PEDIDO_VEND) Adaptador.getItem(pos);
                EliminarRegistros(view, Adaptador, ItemActual.getId(), pos);

            }
        });
        Btn_producto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PEDIDO_VEND ItemActual = (PEDIDO_VEND) Adaptador.getItem(pos);
                int ID = ItemActual.getId();
                CLI_FragmentProductos fr = new CLI_FragmentProductos(2, ID);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.contenedor, fr)
                        .addToBackStack(null)
                        .commit();
                FormAregar.dismiss();
            }
        });
        Btn_cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FormAregar.dismiss();
            }
        });
        FormAregar.show();
    }


    private void HabilitarInput(Boolean estado) {
        InputPago.setEnabled(estado);

    }

    //MOSTRAR
    private void BuscarRegistros(final View view) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("BUSCANDO  ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    LISTA = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(response);
                    //  Integer id = (Integer) jsonObject.get("id");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject OB = jsonArray.getJSONObject(i);
                        int ID = OB.getInt("id");
                        String DNI = OB.getString("dni");
                        String NOMBRE = OB.getString("nombre");
                        String APELLIDO = OB.getString("apellido");
                        String CODIGO = OB.getString("codigo");
                        String DESCRIPCION = OB.getString("descripcion");
                        String FECHA = OB.getString("fecha");
                        Double TOTAL = Double.parseDouble(OB.getString("total"));

                        LISTA.add(new PEDIDO_VEND(ID, DNI, NOMBRE, APELLIDO, CODIGO, DESCRIPCION, FECHA, 0.0, 0.0, TOTAL, true));
                    }
                    CargarLista(view);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "show");
                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
                map.put("Filtros", String.valueOf(Filtros));
                if (Filtros == 1) {
                    String Columna = InputColumna.getSelectedItem().toString();
                    map.put("Columna", Columna);
                    map.put("Valor", String.valueOf(InputValor.getText()));
                }
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }


    //MODIFICAR
    private void ModificarRegistros(final View view, final PEDIDO_VEND obj, final int POSCION) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("ACTUALIZANDO ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonArray = new JSONObject(response);

                    if (VEN_AdaptadorPedido.delete(Adaptador, POSCION)) {
                        Toast.makeText(context,  " VENTA REALIZA", Toast.LENGTH_SHORT).show();
                        FormAregar.dismiss();
                    } else {
                        //mandar a refrescar si no se guardo el registro de manera local
                        Filtros = 0;
                        BuscarRegistros(view);
                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "edi");
                int ID = obj.getId();
                map.put("id", String.valueOf(ID));
                map.put("pago", InputPago.getText().toString());
                map.put("total", InputTotal.getText().toString());
                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }

    //MODIFICAR
    private void EliminarRegistros(final View view, final VEN_AdaptadorPedido Adaptador, final int ID, final int POSCION) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("ELIMINADO..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (VEN_AdaptadorPedido.delete(Adaptador, POSCION)) {
                    Toast.makeText(context, NormbreActividad + " ELIMINAD" + OA, Toast.LENGTH_SHORT).show();
                    FormAregar.dismiss();
                } else {
                    Toast.makeText(context, NormbreActividad + " NO SE ELIMIN" + OA, Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "del");
                map.put("id", String.valueOf(ID));
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }


}
