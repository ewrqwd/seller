package com.example.micro_start.store.Controlador.Administrador;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.micro_start.store.Controlador.SharedPreferencesUsuario;
import com.example.micro_start.store.Controlador.UTILITARIOS;
import com.example.micro_start.store.Modelo.PRODUCTOS;
import com.example.micro_start.store.Modelo.SELECTOR;
import com.example.micro_start.store.Modelo.SISTEMA;
import com.example.micro_start.store.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SuppressLint("ValidFragment")
public class ADM_FragmentProductos extends Fragment {
    static SISTEMA SISTEMA = new SISTEMA();
    private UTILITARIOS Utilitarios = new UTILITARIOS();
    private ListView Lista_View;
    private ADM_AdaptadorProductos Adaptador;
    private Dialog FormAregar;
    private Dialog FormBuscar;
    private String NormbreActividad = "PRODUCTO";
    private String OA = "A";
    private static final String URL = SISTEMA.getURL() + "/Admin/Producto";
    ArrayList<PRODUCTOS> LISTA = new ArrayList<>();
    ArrayList<SELECTOR> SELECTORVENDEDOR = new ArrayList<>();
    ArrayList<SELECTOR> SELECTORCATEGORIA = new ArrayList<>();
    //botones
    TextView TITULO;
    Spinner InputVendedor;
    Spinner InputCategoria;
    EditText InputNombre;
    EditText InputDescripcion;
    EditText InputCodigo;
    EditText InputPrecio;
    Switch SwitchEstado;

    Button Btn_agregar;
    Button Btn_editar;
    Button Btn_modificar;
    Button Btn_eliminar;
    Button Btn_cerrar;
    //filtros
    Spinner InputColumna = null;
    EditText InputValor = null;
    int Filtros = 0;
    int Rol = 0;

    public ADM_FragmentProductos(int i) {
        this.Rol = i;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_global, container, false);
        Lista_View = (ListView) view.findViewById(R.id.ListView);
        TextView View = (TextView) view.findViewById(R.id.form_titulo);
        View.setText("NOMINA DE " + NormbreActividad);
        Lista_View.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DisplayInputDialog(i, view);
            }
        });

        final FloatingActionButton fab_agregar = (FloatingActionButton) view.findViewById(R.id.fab_agregar);
        fab_agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisplayInputDialog(-1, view);
            }
        });
        final FloatingActionButton fab_buscar = (FloatingActionButton) view.findViewById(R.id.fab_buscar);
        fab_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisplayBuscarDialog(-1, view);
            }
        });
        Filtros = 0;
        BuscarRegistros(view);

        return view;
    }

    private void CargarLista(View view) {
        //cargar etiquetas

        Adaptador = new ADM_AdaptadorProductos(view.getContext(), LISTA, Lista_View, Rol);
        Lista_View.setAdapter(Adaptador);

    }

    private void DisplayBuscarDialog(final int pos, final View view) {
        FormBuscar = new Dialog(view.getContext());
        FormBuscar.setContentView(R.layout.busqueda_filtro);
        InputColumna = (Spinner) FormBuscar.findViewById(R.id.InputFiltro);
        InputValor = (EditText) FormBuscar.findViewById(R.id.InputValor);
        String[] letra;
        switch (Rol) {
            case 1:
                letra = new String[]{"vendedor", "categoria", "codigo", "nombre", "descripcion", "precio", "estado"};
                break;
            default:
                letra = new String[]{"categoria", "codigo", "nombre", "descripcion", "precio", "estado"};
                break;
        }
        InputColumna.setAdapter(new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_item, letra));

        Button Btn_cerrar = (Button) FormBuscar.findViewById(R.id.Btn_cerrar);
        Button Btn_buscar = (Button) FormBuscar.findViewById(R.id.Btn_buscar);

        Btn_cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FormBuscar.dismiss();
                Filtros = 0;
            }
        });
        Btn_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Filtros = 1;
                BuscarRegistros(view);
            }
        });
        FormBuscar.show();
    }

    private void DisplayInputDialog(final int pos, final View view) {
        FormAregar = new Dialog(view.getContext());
        FormAregar.setTitle("NOMINA DE " + NormbreActividad);
        FormAregar.setContentView(R.layout.dialogo_admin_producto);
        //cargar etiquetas
        TITULO = (TextView) FormAregar.findViewById(R.id.TextTitle);

        InputVendedor = (Spinner) FormAregar.findViewById(R.id.InputVendedor);
        InputCategoria = (Spinner) FormAregar.findViewById(R.id.InputCategoria);
        //end selector
        TableRow GVendedor = (TableRow) FormAregar.findViewById(R.id.GVendedor);
        switch (Rol) {
            case 1:
                GVendedor.setVisibility(View.VISIBLE);
                ListasSpinnerVendedor(view, SISTEMA.getURL() + "/Admin/Usuario", pos);//CARGAR ESPINNER
                break;
            case 2:
                GVendedor.setVisibility(View.INVISIBLE);
                ListasSpinnerCatgoria(view, SISTEMA.getURL() + "/Admin/Categoria", pos, Integer.parseInt(SharedPreferencesUsuario.BuscarDato("id")));//CARGAR ESPINNER


                break;
        }


    }

    public void CARGARCOONENTES(final int pos, final View view) {

        InputNombre = (EditText) FormAregar.findViewById(R.id.InputNombre);
        InputDescripcion = (EditText) FormAregar.findViewById(R.id.InputDescripcion);
        InputCodigo = (EditText) FormAregar.findViewById(R.id.InputCodigo);
        InputPrecio = (EditText) FormAregar.findViewById(R.id.InputPrecio);
        SwitchEstado = (Switch) FormAregar.findViewById(R.id.SwitchEstado);
        InputVendedor.setEnabled(false);
        InputCategoria.setEnabled(false);
        Btn_agregar = (Button) FormAregar.findViewById(R.id.Btn_agregar);
        Btn_editar = (Button) FormAregar.findViewById(R.id.Btn_editar);
        Btn_modificar = (Button) FormAregar.findViewById(R.id.Btn_modificar);
        Btn_eliminar = (Button) FormAregar.findViewById(R.id.Btn_eliminar);
        Btn_cerrar = (Button) FormAregar.findViewById(R.id.Btn_cerrar);

        Btn_modificar.setVisibility(View.INVISIBLE);
        switch (pos) {
            case -1:
                TITULO.setText("AGREGAR " + NormbreActividad);
                Btn_agregar.setVisibility(View.VISIBLE);
                Btn_editar.setVisibility(View.INVISIBLE);
                Btn_eliminar.setVisibility(View.INVISIBLE);
                HabilitarInput(true);

                break;
            default:
                TITULO.setText("MODIFICAR " + NormbreActividad);
                Btn_agregar.setVisibility(View.INVISIBLE);
                Btn_editar.setVisibility(View.VISIBLE);
                Btn_eliminar.setVisibility(View.VISIBLE);
                //cargar elementos en etiquetas
                PRODUCTOS Item = (PRODUCTOS) Adaptador.getItem(pos);

                try {
                    int Posicion = Utilitarios.BuscarPosion(SELECTORVENDEDOR, Item.getId_Vendedor());
                    InputVendedor.setSelection(Posicion);
                } catch (Exception e) {
                }
                InputPrecio.setText(Item.getPrecio().toString());
                InputNombre.setText(Item.getNombre());
                InputDescripcion.setText(Item.getDescripcion());
                InputCodigo.setText(Item.getCodigo());
                SwitchEstado.setChecked(Item.getEstado());
                break;
        }


        Btn_agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PRODUCTOS obj = new PRODUCTOS();

                obj.setNombre(InputNombre.getText().toString());
                obj.setDescripcion(InputDescripcion.getText().toString());
                obj.setCodigo(InputCodigo.getText().toString());
               try{
                   obj.setPrecio(Double.parseDouble(InputPrecio.getText().toString()));
               }catch (Exception e){

               }
                obj.setEstado(SwitchEstado.isChecked());
                String Categoria = String.valueOf(InputCategoria.getSelectedItem());
                String Vendedor = String.valueOf(InputVendedor.getSelectedItem());
                if (Vendedor != "null"|| Rol ==2)  {
                    if (Categoria != "null")  {

                        String Validar = Utilitarios.ValidarFormularioProducto(obj);
                        if (Validar == "OK") {
                            AgregarRegistros(view, obj);
                        } else {
                            Toast.makeText(view.getContext(), "INGRESE " + Validar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(view.getContext(), "INGRESE CATEGORIA", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(view.getContext(), "INGRESE VENDEDOR", Toast.LENGTH_SHORT).show();

                }
            }
        });

        Btn_editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Btn_modificar.setVisibility(View.VISIBLE);
                Btn_editar.setVisibility(View.INVISIBLE);
                HabilitarInput(true);
            }
        });

        Btn_modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                PRODUCTOS ItemActual = (PRODUCTOS) Adaptador.getItem(pos);
                PRODUCTOS obj = new PRODUCTOS();
                obj.setId(ItemActual.getId());

                obj.setNombre(InputNombre.getText().toString());
                obj.setDescripcion(InputDescripcion.getText().toString());
                obj.setCodigo(InputCodigo.getText().toString());
                obj.setPrecio(Double.parseDouble(InputPrecio.getText().toString()));
                obj.setEstado(SwitchEstado.isChecked());


                String Categoria = String.valueOf(InputCategoria.getSelectedItem());
                String Vendedor = String.valueOf(InputVendedor.getSelectedItem());
                if (Vendedor != "null"|| Rol ==2)  {
                    if (Categoria != "null") {

                        String Validar = Utilitarios.ValidarFormularioProducto(obj);
                        if (Validar == "OK") {
                            HabilitarInput(false);
                            Btn_editar.setVisibility(View.VISIBLE);
                            Btn_modificar.setVisibility(View.INVISIBLE);
                            ModificarRegistros(view, obj, pos);
                        } else {
                            HabilitarInput(true);
                            Toast.makeText(view.getContext(), "INGRESE " + Validar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(view.getContext(), "INGRESE CATEGORIA", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(view.getContext(), "INGRESE VENDEDOR", Toast.LENGTH_SHORT).show();

                }






            }
        });

        Btn_eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PRODUCTOS ItemActual = (PRODUCTOS) Adaptador.getItem(pos);
                EliminarRegistros(view, Adaptador, ItemActual.getId(), pos);

            }
        });

        Btn_cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FormAregar.dismiss();
            }
        });
        FormAregar.show();
    }

    private void Limpiar() {
        InputNombre.setText("");
        InputDescripcion.setText("");
        InputCodigo.setText("");
        InputPrecio.setText("");
        SwitchEstado.setChecked(true);
    }

    private void HabilitarInput(Boolean estado) {
        InputVendedor.setEnabled(estado);
        InputCategoria.setEnabled(estado);
        InputNombre.setEnabled(estado);
        InputDescripcion.setEnabled(estado);
        InputCodigo.setEnabled(estado);
        InputPrecio.setEnabled(estado);
        SwitchEstado.setEnabled(estado);
    }

    //MOSTRAR
    private void BuscarRegistros(final View view) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("BUSCANDO  ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    LISTA = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(response);
                    //  Integer id = (Integer) jsonObject.get("id");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject OB = jsonArray.getJSONObject(i);
                        int ID = OB.getInt("id");
                        String NOMBRE = OB.getString("nombre");
                        String DESCRIPCION = OB.getString("descripcion");
                        String CODIGO = OB.getString("codigo");
                        Double PRECIO = Double.parseDouble(OB.getString("precio"));
                        int ESTADO_INT = OB.getInt("estado");
                        String vendedor = OB.getString("vendedor");
                        int ID_vendedor = OB.getInt("id_vendedor");
                        String CATEGORIA = OB.getString("categoria");
                        int ID_CATEGORIA = OB.getInt("id_categoria");
                        Boolean ESTADO = false;
                        if (ESTADO_INT == 1) {
                            ESTADO = true;
                        }

                        LISTA.add(new PRODUCTOS(ID, CODIGO, NOMBRE, DESCRIPCION, PRECIO, vendedor, ID_vendedor, CATEGORIA, ID_CATEGORIA, ESTADO));
                    }
                    CargarLista(view);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "show");
                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
                map.put("Filtros", String.valueOf(Filtros));
                if (Filtros == 1) {
                    String Columna = InputColumna.getSelectedItem().toString();
                    map.put("Columna", Columna);
                    String Valor = String.valueOf(InputValor.getText());
                    if (Columna == "estado") {
                        if (Valor.equals("HABILITADO")) {
                            Valor = "1";
                        } else {
                            Valor = "0";
                        }
                    }
                    map.put("Valor", Valor);
                }
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }

    //AGREGAR
    private void AgregarRegistros(final View view, final PRODUCTOS obj) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("CREANDO ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonArray = new JSONObject(response);
                    int ID = (int) jsonArray.get("id");
                    obj.setId(ID);
                    try {
                        int id_vendedor = SELECTORVENDEDOR.get(InputVendedor.getSelectedItemPosition()).getIndex();
                        String vendedor = InputVendedor.getSelectedItem().toString();
                        obj.setVendedor(vendedor);
                        obj.setId_Vendedor(id_vendedor);
                    } catch (Exception e) {
                    }
                    try {
                        int id_categoria = SELECTORCATEGORIA.get(InputCategoria.getSelectedItemPosition()).getIndex();
                        String categoria = InputCategoria.getSelectedItem().toString();
                        obj.setCategoria(categoria);
                        obj.setId_Categoria(id_categoria);
                    } catch (Exception e) {
                    }

                    if (ADM_AdaptadorProductos.save(Adaptador, obj)) {
                        Toast.makeText(context, NormbreActividad + " GUARDAD" + OA, Toast.LENGTH_SHORT).show();
                        Limpiar();
                        FormAregar.dismiss();
                    } else {
                        //mandar a refrescar si no se guardo el registro de manera local
                        Filtros = 0;
                        BuscarRegistros(view);
                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "add");
                map.put("nombre", InputNombre.getText().toString());
                map.put("descripcion", InputDescripcion.getText().toString());
                map.put("codigo", InputCodigo.getText().toString());
                map.put("precio", InputPrecio.getText().toString());
                String estado = "0";
                if (SwitchEstado.isChecked()) {
                    estado = "1";
                }
                map.put("estado", estado);

                try {
                    int id_vendedor = SELECTORVENDEDOR.get(InputVendedor.getSelectedItemPosition()).getIndex();
                    map.put("id_vendedor", String.valueOf(id_vendedor));
                } catch (Exception e) {
                }
                try {
                    int id_categoria = SELECTORCATEGORIA.get(InputCategoria.getSelectedItemPosition()).getIndex();
                    map.put("id_categoria", String.valueOf(id_categoria));
                } catch (Exception e) {
                }
                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }

    //MODIFICAR
    private void ModificarRegistros(final View view, final PRODUCTOS obj, final int POSCION) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("ACTUALIZANDO ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonArray = new JSONObject(response);

                    try {
                        int id_vendedor = SELECTORVENDEDOR.get(InputVendedor.getSelectedItemPosition()).getIndex();
                        String vendedor = InputVendedor.getSelectedItem().toString();
                        obj.setVendedor(vendedor);
                        obj.setId_Vendedor(id_vendedor);
                    } catch (Exception e) {
                    }
                    try {
                        int id_categoria = SELECTORCATEGORIA.get(InputCategoria.getSelectedItemPosition()).getIndex();
                        String categoria = InputCategoria.getSelectedItem().toString();
                        obj.setCategoria(categoria);
                        obj.setId_Categoria(id_categoria);
                    } catch (Exception e) {
                    }

                    if (ADM_AdaptadorProductos.update(Adaptador, POSCION, obj)) {
                        Toast.makeText(context, NormbreActividad + " MODIFICAD" + OA, Toast.LENGTH_SHORT).show();
                        Btn_modificar.setVisibility(View.INVISIBLE);
                        Btn_editar.setVisibility(View.VISIBLE);
                    } else {
                        //mandar a refrescar si no se guardo el registro de manera local
                        Filtros = 0;
                        BuscarRegistros(view);
                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "edi");
                int ID = obj.getId();
                map.put("id", String.valueOf(ID));
                map.put("nombre", InputNombre.getText().toString());
                map.put("descripcion", InputDescripcion.getText().toString());
                map.put("codigo", InputCodigo.getText().toString());
                map.put("precio", InputPrecio.getText().toString());
                String estado = "0";
                if (SwitchEstado.isChecked()) {
                    estado = "1";
                }
                map.put("estado", estado);

                try {
                    int id_vendedor = SELECTORVENDEDOR.get(InputVendedor.getSelectedItemPosition()).getIndex();
                    map.put("id_vendedor", String.valueOf(id_vendedor));
                } catch (Exception e) {
                }
                try {
                    int id_categoria = SELECTORCATEGORIA.get(InputCategoria.getSelectedItemPosition()).getIndex();
                    map.put("id_categoria", String.valueOf(id_categoria));
                } catch (Exception e) {
                }
                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);

    }

    //ELIMINAR
    private void EliminarRegistros(final View view, final ADM_AdaptadorProductos Adaptador, final int ID, final int POSCION) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("ELIMINADO..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (ADM_AdaptadorProductos.delete(Adaptador, POSCION)) {
                    Toast.makeText(context, NormbreActividad + " ELIMINAD" + OA, Toast.LENGTH_SHORT).show();
                    FormAregar.dismiss();
                } else {
                    Toast.makeText(context, NormbreActividad + " NO SE ELIMIN" + OA, Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "del");
                map.put("id", String.valueOf(ID));
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }

    public void ListasSpinnerVendedor(final View view, String URL, final int pos) {
        final Context context = view.getContext();

        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("BUSCANDO ..");
        //mostrar el progress
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONArray jsonArray = new JSONArray(response);
                    int T = jsonArray.length();

                    String[] valores = new String[T];
                    SELECTORVENDEDOR = new ArrayList<>();
                    if (T != 0) {
                        for (int i = 0; i < T; i++) {
                            JSONObject OB = jsonArray.getJSONObject(i);
                            String VALUE = OB.getString("value");
                            int INDEX = OB.getInt("index");
                            valores[i] = VALUE;
                            SELECTORVENDEDOR.add(new SELECTOR(INDEX, VALUE));
                        }

                        InputVendedor.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, valores));
                        InputVendedor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                int index = SELECTORVENDEDOR.get(position).getIndex();
                                //aqui e puede obtner el id con este evento
                                ListasSpinnerCatgoria(view, SISTEMA.getURL() + "/Admin/Categoria", 0, index);//CARGAR ESPINNER
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                            }
                        });

                        CARGARCOONENTES(pos, view);
                    } else {

                        Toast.makeText(context, "No se existen vendedores", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "lista");
                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
                map.put("tipo_usuario", "2");//mostrar vendedores
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }

    public void ListasSpinnerCatgoria(final View view, String URL, final int pos, final int id_vendedor) {

        final Context context = view.getContext();
        //crear progress
        final ProgressDialog progressDialog = new ProgressDialog(context);
        //para que no se cancele si se presiona en la pantalla
        progressDialog.setCancelable(false);
        //mensaje que se muestra
        progressDialog.setMessage("BUSCANDO ..");
        //mostrar el progress
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONArray jsonArray = new JSONArray(response);
                    int T = jsonArray.length();

                    String[] valores = new String[T];
                    SELECTORCATEGORIA = new ArrayList<>();
                    InputCategoria.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, valores));

                    if (T != 0) {
                        for (int i = 0; i < T; i++) {
                            JSONObject OB = jsonArray.getJSONObject(i);
                            String VALUE = OB.getString("value");
                            int INDEX = OB.getInt("index");
                            valores[i] = VALUE;
                            SELECTORCATEGORIA.add(new SELECTOR(INDEX, VALUE));
                        }

                        InputCategoria.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, valores));

                        if (Rol == 2) {
                            CARGARCOONENTES(pos, view);
                        }
                    } else {

                        Toast.makeText(context, "No se existen Categorias", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Utilitarios.MensajeError(context, progressDialog, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("oper", "lista");
                map.put("id_vendedor", String.valueOf(id_vendedor));//mostrar vendedores

                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(stringRequest);


    }

}
