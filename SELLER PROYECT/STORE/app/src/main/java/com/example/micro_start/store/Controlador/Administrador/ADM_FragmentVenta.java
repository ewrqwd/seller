//package com.example.micro_start.store.Controlador.Administrador;
//
//import android.annotation.SuppressLint;
//import android.app.DatePickerDialog;
//import android.app.Dialog;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.os.Bundle;
//import android.support.design.widget.FloatingActionButton;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.DatePicker;
//import android.widget.EditText;
//import android.widget.ListView;
//import android.widget.Spinner;
//import android.widget.Switch;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//import com.example.micro_start.store.Controlador.SharedPreferencesUsuario;
//import com.example.micro_start.store.Controlador.UTILITARIOS;
//import com.example.micro_start.store.Modelo.PEDIDO_CLI;
//import com.example.micro_start.store.Modelo.SISTEMA;
//import com.example.micro_start.store.R;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.Locale;
//import java.util.Map;
//
//@SuppressLint("ValidFragment")
//public class ADM_FragmentVenta extends Fragment {
//    static SISTEMA SISTEMA = new SISTEMA();
//    private UTILITARIOS Utilitarios = new UTILITARIOS();
//    private ListView Lista_View;
//    private CLI_AdaptadorPedido Adaptador;
//    private Dialog FormAregar;
//    private Dialog FormBuscar;
//    private String NormbreActividad = "PEDIDO_CLI";
//    private String OA = "A";
//    private static final String URL = SISTEMA.getURL() + "/Admin/Pedido";
//    ArrayList<PEDIDO_CLI> LISTA = new ArrayList<>();
//    //botones
//    TextView TITULO;
//
//    EditText InputCodigo;
//    EditText InputDescripcion;
//    EditText InputFecha;
//    EditText InputTotal;
//    Switch SwitchEstado;
//    final  Calendar calendario = Calendar.getInstance();
//
//    Button Btn_agregar;
//    Button Btn_editar;
//    Button Btn_modificar;
//    Button Btn_eliminar;
//    Button Btn_cerrar;
//    //filtros
//    Spinner InputColumna = null;
//    EditText InputValor = null;
//    int Filtros = 0;
//    int Rol = 0;
//
//    public ADM_FragmentVenta(int i) {
//        this.Rol = i;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater,
//                             ViewGroup container,
//                             Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_global, container, false);
//        Lista_View = (ListView) view.findViewById(R.id.ListView);
//        TextView View = (TextView) view.findViewById(R.id.form_titulo);
//        View.setText("NOMINA DE " + NormbreActividad);
//        Lista_View.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                DisplayInputDialog(i, view);
//            }
//        });
//
//        final FloatingActionButton fab_agregar = (FloatingActionButton) view.findViewById(R.id.fab_agregar);
//        fab_agregar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                DisplayInputDialog(-1, view);
//            }
//        });
//        final FloatingActionButton fab_buscar = (FloatingActionButton) view.findViewById(R.id.fab_buscar);
//        fab_buscar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                DisplayBuscarDialog(-1, view);
//            }
//        });
//        Filtros = 0;
//        BuscarRegistros(view);
//
//        return view;
//    }
//
//    private void CargarLista(View view) {
//        //cargar etiquetas
//
//        Adaptador = new CLI_AdaptadorPedido(view.getContext(), LISTA, Lista_View, Rol);
//        Lista_View.setAdapter(Adaptador);
//
//    }
//
//    private void DisplayBuscarDialog(final int pos, final View view) {
//        FormBuscar = new Dialog(view.getContext());
//        FormBuscar.setContentView(R.layout.busqueda_filtro);
//        InputColumna = (Spinner) FormBuscar.findViewById(R.id.InputFiltro);
//        InputValor = (EditText) FormBuscar.findViewById(R.id.InputValor);
//
//
//        String[] letra = {"codigo", "decripcion"};
//        InputColumna.setAdapter(new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_item, letra));
//
//        Button Btn_cerrar = (Button) FormBuscar.findViewById(R.id.Btn_cerrar);
//        Button Btn_buscar = (Button) FormBuscar.findViewById(R.id.Btn_buscar);
//
//        Btn_cerrar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FormBuscar.dismiss();
//                Filtros = 0;
//            }
//        });
//        Btn_buscar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Filtros = 1;
//                BuscarRegistros(view);
//            }
//        });
//        FormBuscar.show();
//    }
//
//    private void DisplayInputDialog(final int pos, final View view) {
//        FormAregar = new Dialog(view.getContext());
//        FormAregar.setTitle("NOMINA DE " + NormbreActividad);
//        FormAregar.setContentView(R.layout.dialogo_cli_pedido);
//        //cargar etiquetas
//        TITULO = (TextView) FormAregar.findViewById(R.id.TextTitle);
//
//        CARGARCOONENTES(pos, view);
//
//
//    }
//
//
//    public void CARGARCOONENTES(final int pos, final View view) {
//
//        InputCodigo = (EditText) FormAregar.findViewById(R.id.InputCodigo);
//        InputDescripcion = (EditText) FormAregar.findViewById(R.id.InputDescripcion);
//        InputFecha = (EditText) FormAregar.findViewById(R.id.InputFecha);
//
//        InputFecha.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new DatePickerDialog(view.getContext(), date, calendario
//                        .get(Calendar.YEAR), calendario.get(Calendar.MONTH),
//                        calendario.get(Calendar.DAY_OF_MONTH)).show();
//            }
//        });
//
//        InputTotal = (EditText) FormAregar.findViewById(R.id.InputTotal);
//        SwitchEstado = (Switch) FormAregar.findViewById(R.id.SwitchEstado);
//
//        Btn_agregar = (Button) FormAregar.findViewById(R.id.Btn_agregar);
//        Btn_editar = (Button) FormAregar.findViewById(R.id.Btn_editar);
//        Btn_modificar = (Button) FormAregar.findViewById(R.id.Btn_modificar);
//        Btn_eliminar = (Button) FormAregar.findViewById(R.id.Btn_eliminar);
//        Btn_cerrar = (Button) FormAregar.findViewById(R.id.Btn_cerrar);
//
//        Btn_modificar.setVisibility(View.INVISIBLE);
//        switch (pos) {
//            case -1:
//                TITULO.setText("AGREGAR " + NormbreActividad);
//                Btn_agregar.setVisibility(View.VISIBLE);
//                Btn_editar.setVisibility(View.INVISIBLE);
//                Btn_eliminar.setVisibility(View.INVISIBLE);
//                HabilitarInput(true);
//
//                break;
//            default:
//                TITULO.setText("MODIFICAR " + NormbreActividad);
//                Btn_agregar.setVisibility(View.INVISIBLE);
//                Btn_editar.setVisibility(View.VISIBLE);
//                Btn_eliminar.setVisibility(View.VISIBLE);
//                //cargar elementos en etiquetas
//                PEDIDO_CLI Item = (PEDIDO_CLI) Adaptador.getItem(pos);
//                InputCodigo.setText(Item.getCodigo());
//                InputDescripcion.setText(Item.getDescripcion());
//                InputFecha.setText(Item.getFecha());
//                InputTotal.setText(Item.getTotal().toString());
//                SwitchEstado.setChecked(Item.getEstado());
//                break;
//        }
//
//
//        Btn_agregar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                PEDIDO_CLI obj = new PEDIDO_CLI();
//                obj.setCodigo(InputCodigo.getText().toString());
//                obj.setDescripcion(InputDescripcion.getText().toString());
//                obj.setFecha(InputFecha.getText().toString());
//                obj.setTotal(Double.parseDouble(InputTotal.getText().toString()));
//                obj.setEstado(SwitchEstado.isChecked());
//                String Validar = Utilitarios.ValidarFormularioVenta(obj);
//                if (Validar == "OK") {
//                    AgregarRegistros(view, obj);
//                } else {
//                    Toast.makeText(view.getContext(), "INGRESE " + Validar, Toast.LENGTH_SHORT).show();
//                }
//
//
//            }
//        });
//
//        Btn_editar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Btn_modificar.setVisibility(View.VISIBLE);
//                Btn_editar.setVisibility(View.INVISIBLE);
//                HabilitarInput(true);
//            }
//        });
//
//        Btn_modificar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                HabilitarInput(false);
//                PEDIDO_CLI ItemActual = (PEDIDO_CLI) Adaptador.getItem(pos);
//                PEDIDO_CLI obj = new PEDIDO_CLI();
//                obj.setId(ItemActual.getId());
//
//                obj.setCodigo(InputCodigo.getText().toString());
//                obj.setDescripcion(InputDescripcion.getText().toString());
//                SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
//                obj.setFecha(InputFecha.getText().toString());
//                obj.setTotal(Double.parseDouble(InputTotal.getText().toString()));
//                obj.setEstado(SwitchEstado.isChecked());
//                String Validar = Utilitarios.ValidarFormularioVenta(obj);
//                if (Validar == "OK") {
//                    Btn_editar.setVisibility(View.VISIBLE);
//                    Btn_modificar.setVisibility(View.INVISIBLE);
//                    ModificarRegistros(view, obj, pos);
//                } else {
//                    HabilitarInput(true);
//                    Toast.makeText(view.getContext(), "INGRESE " + Validar, Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });
//
//        Btn_eliminar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                PEDIDO_CLI ItemActual = (PEDIDO_CLI) Adaptador.getItem(pos);
//                EliminarRegistros(view, Adaptador, ItemActual.getId(), pos);
//
//            }
//        });
//
//        Btn_cerrar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FormAregar.dismiss();
//            }
//        });
//        FormAregar.show();
//    }
//
//    private void Limpiar() {
//        InputCodigo.setText("");
//        InputDescripcion.setText("");
//       // InputFecha.setText("");
//        InputTotal.setText("");
//        SwitchEstado.setChecked(true);
//    }
//
//    private void HabilitarInput(Boolean estado) {
//        InputCodigo.setEnabled(estado);
//        InputDescripcion.setEnabled(estado);
//        InputFecha.setEnabled(estado);
//        InputTotal.setEnabled(estado);
//        SwitchEstado.setEnabled(estado);
//    }
//
//    //MOSTRAR
//    private void BuscarRegistros(final View view) {
//        final Context context = view.getContext();
//
//        //crear progress
//        final ProgressDialog progressDialog = new ProgressDialog(context);
//        //para que no se cancele si se presiona en la pantalla
//        progressDialog.setCancelable(false);
//        //mensaje que se muestra
//        progressDialog.setMessage("BUSCANDO  ..");
//        //mostrar el progress
//        progressDialog.show();
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                URL
//                , new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try {
//                    LISTA = new ArrayList<>();
//                    JSONArray jsonArray = new JSONArray(response);
//                    //  Integer id = (Integer) jsonObject.get("id");
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject OB = jsonArray.getJSONObject(i);
//                        int ID = OB.getInt("id");
//                        String CODIGO = OB.getString("codigo");
//                        String DESCRIPCION = OB.getString("descripcion");
//                        String FECHA = OB.getString("fecha");
//                        Double TOTAL = Double.parseDouble(OB.getString("total"));
//                        int ESTADO_INT = OB.getInt("estado");
//
//                        Boolean ESTADO = false;
//                        if (ESTADO_INT == 1) {
//                            ESTADO = true;
//                        }
//                        LISTA.add(new PEDIDO_CLI(ID, CODIGO, DESCRIPCION, FECHA, TOTAL, "", 0, "", 0, ESTADO));
//                    }
//                    CargarLista(view);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                progressDialog.dismiss();
//            }
//
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                Utilitarios.MensajeError(context, progressDialog, error);
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> map = new HashMap<>();
//                map.put("oper", "show");
//                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
//                map.put("Filtros", String.valueOf(Filtros));
//                if (Filtros == 1) {
//                    String Columna = InputColumna.getSelectedItem().toString();
//                    map.put("Columna", Columna);
//                    map.put("Valor", String.valueOf(InputValor.getText()));
//                }
//                return map;
//            }
//        };
//        RequestQueue queue = Volley.newRequestQueue(context);
//        queue.add(stringRequest);
//
//
//    }
//
//    //AGREGAR
//    private void AgregarRegistros(final View view, final PEDIDO_CLI obj) {
//        final Context context = view.getContext();
//
//        //crear progress
//        final ProgressDialog progressDialog = new ProgressDialog(context);
//        //para que no se cancele si se presiona en la pantalla
//        progressDialog.setCancelable(false);
//        //mensaje que se muestra
//        progressDialog.setMessage("CREANDO ..");
//        //mostrar el progress
//        progressDialog.show();
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                URL
//                , new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try {
//                    JSONObject jsonArray = new JSONObject(response);
//                    int ID = (int) jsonArray.get("id");
//                    obj.setId(ID);
//
//
//                    if (CLI_AdaptadorPedido.save(Adaptador, obj)) {
//                        Toast.makeText(context, NormbreActividad + " GUARDAD" + OA, Toast.LENGTH_SHORT).show();
//                        Limpiar();
//                        FormAregar.dismiss();
//                    } else {
//                        //mandar a refrescar si no se guardo el registro de manera local
//                        Filtros = 0;
//                        BuscarRegistros(view);
//                    }
//                    progressDialog.dismiss();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                Utilitarios.MensajeError(context, progressDialog, error);
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> map = new HashMap<>();
//                map.put("oper", "add");
//                map.put("codigo", InputCodigo.getText().toString());
//                map.put("descripcion", InputDescripcion.getText().toString());
//                map.put("fecha", InputFecha.getText().toString());
//                map.put("total", InputTotal.getText().toString());
//                String estado = "0";
//                if (SwitchEstado.isChecked()) {
//                    estado = "1";
//                }
//                map.put("estado", estado);
//                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
//                return map;
//            }
//        };
//        RequestQueue queue = Volley.newRequestQueue(context);
//        queue.add(stringRequest);
//
//
//    }
//
//    //MODIFICAR
//    private void ModificarRegistros(final View view, final PEDIDO_CLI obj, final int POSCION) {
//        final Context context = view.getContext();
//
//        //crear progress
//        final ProgressDialog progressDialog = new ProgressDialog(context);
//        //para que no se cancele si se presiona en la pantalla
//        progressDialog.setCancelable(false);
//        //mensaje que se muestra
//        progressDialog.setMessage("ACTUALIZANDO ..");
//        //mostrar el progress
//        progressDialog.show();
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                URL
//                , new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try {
//                    JSONObject jsonArray = new JSONObject(response);
//
//                    if (CLI_AdaptadorPedido.update(Adaptador, POSCION, obj)) {
//                        Toast.makeText(context, NormbreActividad + " MODIFICAD" + OA, Toast.LENGTH_SHORT).show();
//                        Btn_modificar.setVisibility(View.INVISIBLE);
//                        Btn_editar.setVisibility(View.VISIBLE);
//                    } else {
//                        //mandar a refrescar si no se guardo el registro de manera local
//                        Filtros = 0;
//                        BuscarRegistros(view);
//                    }
//                    progressDialog.dismiss();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                Utilitarios.MensajeError(context, progressDialog, error);
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> map = new HashMap<>();
//                map.put("oper", "edi");
//                int ID = obj.getId();
//                map.put("id", String.valueOf(ID));
//                map.put("codigo", InputCodigo.getText().toString());
//                map.put("descripcion", InputDescripcion.getText().toString());
//                map.put("fecha", InputFecha.getText().toString());
//                map.put("total", InputTotal.getText().toString());
//                String estado = "0";
//                if (SwitchEstado.isChecked()) {
//                    estado = "1";
//                }
//                map.put("estado", estado);
//                map.put("id_usuario", SharedPreferencesUsuario.BuscarDato("id"));
//                return map;
//            }
//        };
//        RequestQueue queue = Volley.newRequestQueue(context);
//        queue.add(stringRequest);
//
//
//    }
//
//    //MODIFICAR
//    private void EliminarRegistros(final View view, final CLI_AdaptadorPedido Adaptador, final int ID, final int POSCION) {
//        final Context context = view.getContext();
//
//        //crear progress
//        final ProgressDialog progressDialog = new ProgressDialog(context);
//        //para que no se cancele si se presiona en la pantalla
//        progressDialog.setCancelable(false);
//        //mensaje que se muestra
//        progressDialog.setMessage("ELIMINADO..");
//        //mostrar el progress
//        progressDialog.show();
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                URL
//                , new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                if (CLI_AdaptadorPedido.delete(Adaptador, POSCION)) {
//                    Toast.makeText(context, NormbreActividad + " ELIMINAD" + OA, Toast.LENGTH_SHORT).show();
//                    FormAregar.dismiss();
//                } else {
//                    Toast.makeText(context, NormbreActividad + " NO SE ELIMIN" + OA, Toast.LENGTH_SHORT).show();
//                }
//                progressDialog.dismiss();
//
//            }
//
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                Utilitarios.MensajeError(context, progressDialog, error);
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> map = new HashMap<>();
//                map.put("oper", "del");
//                map.put("id", String.valueOf(ID));
//                return map;
//            }
//        };
//        RequestQueue queue = Volley.newRequestQueue(context);
//        queue.add(stringRequest);
//
//
//    }
//    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
//
//        @Override
//        public void onDateSet(DatePicker view, int year, int monthOfYear,
//                              int dayOfMonth) {
//            // TODO Auto-generated method stub
//
//            calendario.set(Calendar.YEAR, year);
//            calendario.set(Calendar.MONTH, monthOfYear);
//            calendario.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//            actualizarInput();
//        }
//
//    };
//    private void actualizarInput() {
//        String formatoDeFecha = "dd-MM-yyyy"; //In which you need put here
//        SimpleDateFormat sdf = new SimpleDateFormat(formatoDeFecha, Locale.US);
//
//        InputFecha.setText(sdf.format(calendario.getTime()));
//    }
//
//}
